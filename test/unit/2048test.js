/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


describe('testGridInit', function () {
    it('test grid initialization', function () {
        var grid = new Grid();
        expect(grid.grid[1][2].number).toEqual(0);
        expect(grid.grid[1][0].number).toEqual(0);
        expect(grid.grid[2][2].number).toEqual(0);
    });
});

describe('testaddNumber', function () {
    it('test adding numbers into grid', function () {
        var grid = new Grid();
        grid.addNumber(0, 0, 2);
        grid.addNumber(1, 2, 5);

        expect(grid.grid[0][0].number).toEqual(2);
        expect(grid.grid[1][2].number).toEqual(5);
    });
});

describe('testMoveNumberToEnd', function () {
    it('test moving numbers to end', function () {
        var grid = new Grid();
        grid.addNumber(3, 0, 2);
        grid.addNumber(2, 1, 5);
        grid.addNumber(0, 0, 5);
        grid.moveNumberToEnd(3, 0, Direction.LEFT);
        grid.moveNumberToEnd(2, 1, Direction.LEFT);

        expect(grid.grid[0][0].number).toEqual(5);
        expect(grid.grid[1][0].number).toEqual(2);
        expect(grid.grid[0][1].number).toEqual(5);
    });
});

describe('testGrid Moved', function () {
    it('should return true if the grid is different, and false elsewise', function () {
        var grid1 = new Grid();
        grid1.addNumber(3, 0, 2);
        grid1.addNumber(2, 1, 5);
        grid1.addNumber(0, 0, 5);

        var grid2 = new Grid();
        grid2.addNumber(3, 1, 2);
        grid2.addNumber(2, 2, 5);
        grid2.addNumber(1, 1, 3);

        expect(grid1.didMoved(grid1, grid2)).toEqual(true);
        expect(grid1.didMoved(grid1, grid1)).toEqual(false);
        expect(grid2.didMoved(grid2, grid2)).toEqual(false);

    });
});


describe('testHasNumberOnSide', function () {
    it('returns true if grid has number counting from direction', function () {
        var grid = new Grid();
        grid.addNumber(0, 0, 2);
        expect(grid.hasNumberOnSide(0, 0)).toEqual(false);
        grid.addNumber(0, 1, 5);
        expect(grid.hasNumberOnSide(0, 0)).toEqual(false);
        expect(grid.hasNumberOnSide(0, 1)).toEqual(false);
        grid.addNumber(2, 1, 5);
        expect(grid.hasNumberOnSide(2, 1)).toEqual(true);
        grid.addNumber(3, 1, 5);
        expect(grid.hasNumberOnSide(3, 1)).toEqual(true);
    });
});

describe('testMergeOrMoveNumber', function () {
    it('merge if mergable, if not mergable then move to side of number', function () {
        var grid = new Grid();
        //mergable number
        grid.addNumber(0, 0, 2);
        grid.addNumber(2, 0, 2);
        grid.mergeOrMoveNumber(2, 0);
        expect(grid.grid[0][0].number).toEqual(3);
        expect(grid.grid[1][0].number).toEqual(0);
        expect(grid.grid[2][0].number).toEqual(0);

        grid.addNumber(1, 0, 3);
        grid.addNumber(2, 0, 4);
        grid.mergeOrMoveNumber(1, 0);
        grid.mergeOrMoveNumber(2, 0);
        expect(grid.grid[0][0].number).toEqual(5);
        expect(grid.grid[2][0].number).toEqual(0);
        expect(grid.grid[1][0].number).toEqual(0);

        var grid = new Grid();
        grid.addNumber(1, 2, 5);
        grid.addNumber(3, 2, 1);
        grid.mergeOrMoveNumber(3, 2);
        expect(grid.grid[1][2].number).toEqual(5);
        expect(grid.grid[3][2].number).toEqual(0);
        expect(grid.grid[2][2].number).toEqual(1);

        var grid = new Grid();
        grid.addNumber(0, 0, 1);
        grid.addNumber(1, 0, 2);
        grid.addNumber(3, 0, 1);
        grid.mergeOrMoveNumber(3, 0);
        expect(grid.grid[0][0].number).toEqual(1);
        expect(grid.grid[1][0].number).toEqual(2);
        expect(grid.grid[2][0].number).toEqual(1);
        expect(grid.grid[3][0].number).toEqual(0);

        var grid = new Grid();
        grid.addNumber(1, 2, 5);
        grid.addNumber(3, 2, 1);
        grid.mergeOrMoveNumber(3, 2);
        expect(grid.grid[1][2].number).toEqual(5);
        expect(grid.grid[3][2].number).toEqual(0);
        expect(grid.grid[2][2].number).toEqual(1);
    });
});

describe('testMakeMove', function () {
    it('check grid correctly shifts numbers', function () {

        //check one number
        var grid = new Grid();
        grid.addNumber(0, 0, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][0].number).toEqual(2);

        var grid = new Grid();
        grid.addNumber(0, 0, 2);
        grid.move(Direction.RIGHT);
        expect(grid.grid[3][0].number).toEqual(2);
        expect(grid.grid[0][0].number).toEqual(0);

        var grid = new Grid();
        grid.addNumber(3, 0, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][0].number).toEqual(2);

        var grid = new Grid();
        grid.addNumber(3, 0, 2);
        grid.addNumber(2, 1, 2);
        grid.move(Direction.DOWN);
        expect(grid.grid[3][3].number).toEqual(2);
        expect(grid.grid[2][3].number).toEqual(2);

        //check two number merge
        var grid = new Grid();
        grid.addNumber(0, 0, 2);
        grid.addNumber(2, 0, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][0].number).toEqual(3);
        expect(grid.grid[1][0].number).toEqual(0);
        expect(grid.grid[2][0].number).toEqual(0);

        //check triple numbers
        grid = new Grid();
        grid.addNumber(1, 0, 2);
        grid.addNumber(2, 0, 2);
        grid.addNumber(3, 0, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][0].number).toEqual(3);
        expect(grid.grid[1][0].number).toEqual(2);
        expect(grid.grid[2][0].number).toEqual(0);

        grid = new Grid();
        grid.addNumber(0, 0, 2);
        grid.addNumber(1, 0, 3);
        grid.addNumber(3, 0, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][0].number).toEqual(2);
        expect(grid.grid[1][0].number).toEqual(3);
        expect(grid.grid[2][0].number).toEqual(2);

        //check four number merge
        grid = new Grid();
        grid.addNumber(0, 1, 2);
        grid.addNumber(1, 1, 2);
        grid.addNumber(2, 1, 2);
        grid.addNumber(3, 1, 2);
        grid.move(Direction.LEFT);
        expect(grid.grid[0][1].number).toEqual(3);
        expect(grid.grid[1][1].number).toEqual(3);
        expect(grid.grid[2][1].number).toEqual(0);
        expect(grid.grid[3][1].number).toEqual(0);
    });
});