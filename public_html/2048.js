//directions ENUM. use string as identifier for enums in case need to print out
var Direction = {
    LEFT: "LEFT",
    RIGHT: "RIGHT",
    UP: "UP",
    DOWN: "DOWN"
};
var GRID_SIZE = 4;
function Game() {
    var Status = {
        PLAYING: "PLAYING",
        GAMEOVER: "GAMEOVER"
    };
    this.grid = new Grid();
    this.grid.spawn();
    this.status = Status.PLAYING;
    //INITIALIZE DOM FIELD CONTAINER
    this.viewGrid = Array(GRID_SIZE);
    for (var x = 0; x < GRID_SIZE; x++) {
        this.viewGrid [x] = new Array(GRID_SIZE);
        for (var y = 0; y < GRID_SIZE; y++) {
            this.viewGrid [x][y] = 0;
        }
    }
    //populate DOM
    var gameDiv = document.createElement("div");
    gameDiv.setAttribute("class", "gameDiv");
    document.getElementById("touch").appendChild(gameDiv);
    for (var y = 0; y < GRID_SIZE; y++) {
        var row = document.createElement("div");
        row.setAttribute("class", "row")
        gameDiv.appendChild(row);
        for (var x = 0; x < GRID_SIZE; x++) {
            var block = document.createElement("div");
            block.setAttribute("class", "block");
            row.appendChild(block);
            this.viewGrid[x][y] = document.createElement("span");
            this.viewGrid[x][y].setAttribute("class", "block");
            block.appendChild(this.viewGrid[x][y]);
        }
    }

    this.gameOver = function () {
        this.status = Status.GAMEOVER;
        var splash = document.getElementById("gameoverDiv");
        splash.style.display = "block";
    }

    this.refreshView = function () {
        for (var x = 0; x < GRID_SIZE; x++) {
            for (var y = 0; y < GRID_SIZE; y++) {
                this.viewGrid[x][y].innerHTML = this.grid.grid[x][y].displayableNumber();
                this.viewGrid[x][y].setAttribute("number", this.grid.grid[x][y].displayableNumber());
            }
        }
    }

    this.refreshView();
    this.onPlayerSwipe = function (swipeDirection) {
        if (this.status == Status.PLAYING) { //game in progress
            //handle user input
            var gridMoved = this.grid.move(swipeDirection);
            //spawn some blocks if the grid has been moved (prevent players spamming same direction)
            if (gridMoved) {
                this.grid.spawn();
            }
            //update the view
            this.refreshView();
            //turn end
            //check if player has lost
            if (this.grid.checkGameOver()) {
                this.gameOver();
            }
        } else {//game not in progress
            //ignore user input
        }
    }


}

//GRID CLASS
function Grid() {
    this.grid = initializeGrid();
    this.checkGameOver = function () {
        //returns boolean true or false
        if (this.isFull()) {
            //check each block to see if mergable with neighbour blocks
            //iterate though grid
            for (var x = 0; x < GRID_SIZE; x++) {
                for (var y = 0; y < GRID_SIZE; y++) {
                    var number = this.grid[x][y];
                    if (x != 0) {
                        //not leftmost-side block
                        if (number.mergeableWith(this.grid[x - 1][y])) {
                            return false;
                        }
                    }
                    if (y != 0) {
                        //not top row block
                        if (number.mergeableWith(this.grid[x][y - 1])) {
                            return false;
                        }
                    }
                    if (x != GRID_SIZE - 1) {
                        //not rightmost-side block
                        if (number.mergeableWith(this.grid[x + 1][y])) {
                            return false;
                        }
                    }
                    if (y != GRID_SIZE - 1) {
                        //not bottom row block
                        if (number.mergeableWith(this.grid[x][y + 1])) {
                            return false;
                        }
                    }
                }
            }
        } else {
            //grid still has space
            return false;
        }
        return true;
    }

    this.isFull = function () {
        //iterate though grid
        for (var x = 0; x < GRID_SIZE; x++) {
            for (var y = 0; y < GRID_SIZE; y++) {
                //return false if found empty block
                if (!this.grid[x][y].hasNumber())
                    return false;
            }
        }
        return true;
    }

    this.rotate90CounterClockwise = function () {
//make a copy of the GRID array
        var newGrid = [];
        for (var i = 0; i < this.grid.length; i++)
            newGrid[i] = this.grid[i].slice();
        //rotate the grid
        for (var i = 0; i < GRID_SIZE; i++) {
            for (var j = 0; j < GRID_SIZE; j++) {
                newGrid[i][j] = this.grid[GRID_SIZE - j - 1][i];
            }
        }
//assign the rotated grid to GRID 
        this.grid = newGrid;
    }

    this.move = function (swipeDirection) {

//make a copy of the grid (to check if grid has moved later)
        var oldGrid = [];
        for (var i = 0; i < this.grid.length; i++)
            oldGrid[i] = this.grid[i].slice();
        //rotate grid so that player movement is performed as Direction.LEFT
        this.convertDirection(swipeDirection, true);
        //iterate through rotated grid
        for (var y = 0; y < GRID_SIZE; y++)
            for (var x = 0, didMerged = false; x < GRID_SIZE; x++) {
                if ((this.grid[x][y]).hasNumber()) { //there is number on this position!
//move number according to swipe direction
                    if (!this.hasNumberOnSide(x, y, swipeDirection) || didMerged) {//no number blocking
                        didMerged = false;
                        //move number to end
                        this.moveNumberToEnd(x, y, swipeDirection);
                    } else {//number blocking move 
//try to merge number                        
                        didMerged = this.mergeOrMoveNumber(x, y, swipeDirection);
                    }
                }
            }

//revert grid rotation
        this.convertDirection(swipeDirection, false);
        //check if grid has been moved
        return this.didMoved(oldGrid, this.grid);
    }

//compares two 2D arrays to see if they are exactly equal
    this.didMoved = function (oldGrid, newGrid) {
        for (var x = 0; x < oldGrid.length; x++) {
            for (var y = 0; y < oldGrid.length; y++) {
//check if there is change
                if (oldGrid[x][y].number != newGrid[x][y].number) {
//return true if there is a change between the grids
                    return true;
                }
            }
        }
//return false if both grids are similar (player did not move)
        return false;
    }

    this.convertDirection = function (convertFromDirection, isConvertNotRevert) {
//converts all directions into DIRECTION.LEFT by rotating the grid
        if (isConvertNotRevert) { //CONVERT
            switch (convertFromDirection) {
                case Direction.LEFT:
                    break;
                case Direction.RIGHT:
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    break;
                case Direction.UP:
                    this.rotate90CounterClockwise();
                    break;
                case Direction.DOWN:
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    break;
            }
        } else { //REVERT
            switch (convertFromDirection) {
                case Direction.LEFT:
                    break;
                case Direction.RIGHT:
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    break;
                case Direction.UP:
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    this.rotate90CounterClockwise();
                    break;
                case Direction.DOWN:
                    this.rotate90CounterClockwise();
                    break;
            }
        }
    }

    this.moveNumberToEnd = function (fromX, fromY) {
        var toX = fromX, toY = fromY;
        while (toX > 0) {
            toX--;
            if ((this.grid[toX][toY]).hasNumber()) {
                toX++;
                break;
            }
        }
        this.moveNumber(fromX, fromY, toX, toY);
    }


    this.hasNumberOnSide = function (x, y) {
        while (x > 0) {
            x--;
            if ((this.grid[x][y]).hasNumber()) {
                return true;
            }
        }
        return false;
    }
    this.mergeOrMoveNumber = function (x, y) {
        number = this.grid[x][y];
        var merged = false;
        for (var loopX = x - 1; loopX >= 0; loopX--) { //loop from right to left to find blocking number
            if (this.grid[loopX][y].hasNumber()) { //found blocking number
                if (number.mergeableWith(this.grid[loopX][y])) { //check if can merge with blocking number
//merge with number
                    this.moveNumber(x, y, loopX, y);
                    this.grid[loopX][y].merged();
                    //set merged flag to true to prevent double mergings
                    merged = true;
                    //done job, break from for loop
                    loopX = -1;
                }
                else {
//move to side of number
                    this.moveNumber(x, y, loopX + 1, y);
                    //moved number, break from for loop
                    loopX = -1;
                }
            }
        }
//return merge flag
        return merged;
    }

    this.moveNumber = function (fromX, fromY, toX, toY) {
        if (fromX != toX || fromY != toY) { //if source and destination are different
            this.grid[toX][toY] = new Number(this.grid[fromX][fromY].number);
            this.grid[fromX][fromY].destroy();
        }
    }

    this.spawn = function () {
        if (!this.isFull()) {
            var x, y;
            do {
                x = Math.floor(Math.random() * 4);
                y = Math.floor(Math.random() * 4);
                var hasNumber = this.grid[x][y].hasNumber();
            } while (hasNumber);
            this.addNumber(x, y, 1);
        }
    }

    this.isFull = function () {
        for (var y = 0; y < GRID_SIZE; y++)
            for (var x = 0; x < GRID_SIZE; x++) {
                if (!this.grid[x][y].hasNumber())
                    return false;
            }
        return true;
    }

    this.addNumber = function (x, y, number) {
        this.grid[x][y] = new Number(number);
    }

    function initializeGrid() {
//return 2d array of Numbers
        var grid = new Array(GRID_SIZE);
        for (var x = 0; x < GRID_SIZE; x++) {
            grid[x] = new Array(GRID_SIZE);
            for (var y = 0; y < GRID_SIZE; y++)
                grid[x][y] = new Number(0);
        }
        return grid;
    }
}



//NUMBER CLASS
function Number(number) {
    this.number = number;
    this.hasNumber = function () {
        //returns true if number is 1 or more, false if number is 0 (just initialized)
        return (this.number > 0);
    }

    this.displayableNumber = function () {
        if (this.number != 0)
            return Math.pow(2, this.number);
        else
            return "";
    }

    this.mergeableWith = function (otherNumber) {
        return (this.number == otherNumber.number);
    }

    this.merged = function () {
        this.number += 1;
        return this.number;
    }

    this.destroy = function () {
        this.number = 0;
    }
}